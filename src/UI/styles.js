import EStyleSheet from 'react-native-extended-stylesheet';
import { Dimensions } from 'react-native';

const VIEW_WIDTH = Dimensions.get('window').width;

export const iconButtonStyles = EStyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    width: 44,
  },
  image: {
    width: 25,
    height: 25,
    resizeMode: 'cover',
  },
});

export const iconLabelButtonStyles = EStyleSheet.create({
  $centeredStyle: {
    fontSize: 10,
    marginLeft: 10,
  },

  $horizontalContainer: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    height: 60,
  },
  $verticalContainer: {
    justifyContent: 'space-around',
    alignItems: 'center',
    height: 100,
    width: (VIEW_WIDTH - 100) / 2,
    paddingTop: 15,
    paddingBottom: 15,
  },
  containerCentered: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
  },
  image: {
    width: 25,
    height: 25,
  },
  label: {
    letterSpacing: 1,
  },
});

export const settingsListItemStyles = EStyleSheet.create({
  container: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    width: VIEW_WIDTH,
    height: 60,
    marginBottom: 4,
    paddingLeft: 50,
    paddingRight: 50,
  },
  icon: {
    width: 25,
    height: 25,
    marginRight: 25,
    marginLeft: 25,
  },
  title: {
    width: VIEW_WIDTH - 100 - 75 - 75,
    fontSize: 14,
    textAlign: 'left',
  },
  linked: {
    width: 50,
    fontSize: 9,
    textAlign: 'center',
    marginRight: 12,
  },
  rightView: {
    flex: 1,
    alignItems: 'flex-end',
    justifyContent: 'center',
    paddingRight: 12,
  },
});
