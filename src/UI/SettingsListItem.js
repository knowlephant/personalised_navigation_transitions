import React, { useContext } from 'react';
import { View, Text, Image, Switch } from 'react-native';

import { Context } from '../context/store';

import { settingsListItemStyles as styles } from './styles';

const arrowRightIcon = require('../../assets/icons/arrowRight.png');

const SettingsListItem = ({ item }) => {
  const { state, dispatch } = useContext(Context);

  const toggleSwitch = () => {
    dispatch({
      type: 'SET_NAVIGATION_TRANSITION_TYPE',
      value: !state.enableCustomNavigation,
    });
  };

  const renderRightView = (type) => {
    switch (type) {
      case 'NAVIGATE':
        return <Image source={arrowRightIcon} style={styles.icon} />;
      case 'SWITCH':
        return (
          <View style={styles.rightView}>
            <Switch
              trackColor={{ false: '#767577', true: styles.$active }}
              thumbColor={
                state.enableCustomNavigation
                  ? styles.$activeBackground
                  : '#f4f3f4'
              }
              ios_backgroundColor="#3e3e3e"
              onValueChange={toggleSwitch}
              value={state.enableCustomNavigation}
            />
          </View>
        );
      default:
        break;
    }
  };

  return (
    <View style={styles.container}>
      <Text style={styles.title}>{item.title}</Text>
      {renderRightView(item.type)}
    </View>
  );
};

export default SettingsListItem;
