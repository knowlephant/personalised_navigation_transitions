import React from 'react';
import { View, Text, Image, TouchableOpacity } from 'react-native';

import { iconLabelButtonStyles as styles } from './styles';

const IconLabelButton = ({
  onPress,
  icon,
  label,
  tintColor,
  isVertical,
  isCentered,
  disabled,
  height,
  uppercase,
}) => (
  <TouchableOpacity onPress={onPress} disabled={disabled}>
    {isCentered ? (
      <View style={styles.containerCentered}>
        <Image source={icon} style={[styles.image, { tintColor }]} />
        <Text style={[styles.label, styles.$centeredStyle]}>{label}</Text>
      </View>
    ) : (
      <View
        style={[
          isVertical ? styles.$verticalContainer : styles.$horizontalContainer,
          { height },
        ]}
      >
        <Text
          style={[styles.label, uppercase && { textTransform: 'uppercase' }]}
        >
          {label}
        </Text>
        <Image source={icon} style={[styles.image, { tintColor }]} />
      </View>
    )}
  </TouchableOpacity>
);

IconLabelButton.defaultProps = {
  tintColor: '#020202',
  isVertical: false,
  isCentered: false,
  disabled: false,
  height: 60,
  uppercase: false,
  icon: null,
};

export default IconLabelButton;
