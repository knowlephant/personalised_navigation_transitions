import React from 'react';
import { View, Image, TouchableOpacity } from 'react-native';

import { iconButtonStyles as styles } from './styles';

const IconButton = ({ onPress, icon, tintColor, size }) => (
  <TouchableOpacity onPress={onPress}>
    <View style={styles.container}>
      <Image
        source={icon}
        style={[styles.image, { tintColor, width: size, height: size }]}
      />
    </View>
  </TouchableOpacity>
);

export default IconButton;
