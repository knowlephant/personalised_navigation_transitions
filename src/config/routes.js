import React, { useContext } from 'react';
import {
  NavigationContainer,
  DefaultTheme,
  getFocusedRouteNameFromRoute,
} from '@react-navigation/native';
import {
  createStackNavigator,
  CardStyleInterpolators,
  HeaderStyleInterpolators,
} from '@react-navigation/stack';

import IconButton from '../UI/IconButton';

import Explore from '../screens/Explore';
import Gallery from '../screens/Gallery';
import Profile from '../screens/Profile';
import Settings from '../screens/Settings';

import Navigation from '../screens/Navigation';

import { Context } from '../context/store';

import {
  cardStyleInterpolatorHelper,
  headerStyleInterpolatorHelper,
} from './transitionHelpers';

const closeIcon = require('../../assets/icons/close.png');
const menuIcon = require('../../assets/icons/menu.png');

// THIS REMOVES THE HEADER BOTTOM BORDER FOR IOS AND ANDROID
const styles = {
  header: {
    borderBottomWidth: 0,
    shadowColor: 'transparent',
    elevation: 0,
  },
};

// BECAUSE WE DON'T WANT TO DISPLAT THE CONTAINER STACK TITLE
// WE CAN GET THE ROUTE NAME FROM THE HELPER FUNCTION PROVIDED
// BY REACT NAVIGATION
const getHeaderTitle = (route) => {
  const routeName = getFocusedRouteNameFromRoute(route) || 'Explore';
  return routeName;
};

const getHeaderLeftHelper = (route, navigation) => {
  // BECAUSE WE'RE NAVIGATING BETWEEN NESTED CONTAINER STACKS
  // INSIDE THE 'NAVGATE' METHOD WE MUST FIRST INDICATE
  // WHICH STACK WE NAVIGATE TO AND THEN POINT OUT
  // WHICH SCREEN INSIDE THE OPTIONS OBJECT
  if (route.name === 'Main') {
    return (
      <IconButton
        icon={menuIcon}
        tintColor="black"
        size={22}
        onPress={() =>
          navigation.navigate('Popup', {
            screen: 'Navigation',
          })
        }
      />
    );
  } else if (route.name === 'Popup') {
    return (
      <IconButton
        icon={closeIcon}
        tintColor="black"
        size={22}
        onPress={() => navigation.goBack()}
      />
    );
  }
};

const MainNavigationStack = createStackNavigator();
const PopupNavigationStack = createStackNavigator();
const RootNavigationStack = createStackNavigator();

const PopupStackScreen = () => {
  return (
    <PopupNavigationStack.Navigator>
      <PopupNavigationStack.Screen name="Navigation" component={Navigation} />
    </PopupNavigationStack.Navigator>
  );
};

const MainStackScreen = () => {
  const { state } = useContext(Context);

  return (
    <MainNavigationStack.Navigator
      screenOptions={{
        animationEnabled: !state.enableCustomNavigation,
        headerStyleInterpolator: HeaderStyleInterpolators.forStatic,
        cardStyleInterpolator: CardStyleInterpolators.forHorizontalIOS,
      }}
    >
      <MainNavigationStack.Screen name="Explore" component={Explore} />
      <MainNavigationStack.Screen name="Gallery" component={Gallery} />
      <MainNavigationStack.Screen name="Profile" component={Profile} />
      <MainNavigationStack.Screen name="Settings" component={Settings} />
    </MainNavigationStack.Navigator>
  );
};

const RootStackScreen = () => {
  const { state } = useContext(Context);

  return (
    <RootNavigationStack.Navigator>
      <RootNavigationStack.Screen
        name="Main"
        component={MainStackScreen}
        options={({ route, navigation }) => ({
          headerStyle: styles.header,
          headerLeft: () => getHeaderLeftHelper(route, navigation),
          headerTitle: getHeaderTitle(route),
          headerStyleInterpolator: state.enableCustomNavigation
            ? (props) => headerStyleInterpolatorHelper(props)
            : HeaderStyleInterpolators.forStatic,
        })}
      />
      <RootNavigationStack.Screen
        name="Popup"
        component={PopupStackScreen}
        options={({ route, navigation }) => ({
          headerTransparent: true,
          headerStyle: styles.header,
          headerLeft: () => getHeaderLeftHelper(route, navigation),
          headerTitle: getHeaderTitle(route),
          headerStyleInterpolator: state.enableCustomNavigation
            ? (props) => headerStyleInterpolatorHelper(props)
            : HeaderStyleInterpolators.forStatic,
          cardStyleInterpolator: state.enableCustomNavigation
            ? (props) => cardStyleInterpolatorHelper(props)
            : CardStyleInterpolators.forVerticalIOS,
        })}
      />
    </RootNavigationStack.Navigator>
  );
};

const NavigationContainerStack = () => {
  const theme = {
    ...DefaultTheme,
    colors: {
      ...DefaultTheme.colors,
      background: 'white',
    },
  };

  return (
    <NavigationContainer theme={theme}>
      <RootStackScreen />
    </NavigationContainer>
  );
};

export default NavigationContainerStack;
