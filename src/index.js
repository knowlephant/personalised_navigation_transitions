import React from 'react';
import { YellowBox, View } from 'react-native';
import EStyleSheet from 'react-native-extended-stylesheet';

import NavigationContainer from './config/routes';
import Store from './context/store';

YellowBox.ignoreWarnings(['Remote debugger']);

export default function App() {
  return (
    <Store>
      <NavigationContainer />
    </Store>
  );
}

EStyleSheet.build();
