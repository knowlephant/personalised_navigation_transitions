const Reducer = (state, action) => {
  switch (action.type) {
    case 'SET_NAVIGATION_TRANSITION_TYPE':
      return {
        ...state,
        enableCustomNavigation: action.value,
      };
    default:
      return state;
  }
};

export default Reducer;
