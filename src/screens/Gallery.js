import React from 'react';
import { View, Text } from 'react-native';

import styles from './styles';

const Gallery = () => {
  return (
    <View style={styles.container}>
      <Text>Gallery Screen</Text>
    </View>
  );
};

export default Gallery;
