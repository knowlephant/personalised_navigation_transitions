import React from 'react';
import { View, Text, FlatList } from 'react-native';

import SettingsListItem from '../UI/SettingsListItem';

import styles from './styles';

export const SETTINGS_ITEMS = [
  {
    id: 0,
    title: 'Edit Profile',
    type: 'NAVIGATE',
    navigateTo: '',
  },
  {
    id: 1,
    title: 'Change Password',
    type: 'NAVIGATE',
    navigateTo: '',
  },
  {
    id: 2,
    title: 'Zoom Navigation',
    navigateTo: '',
    type: 'SWITCH',
    switchSettings: ['default', 'zoom'],
  },
];

const Settings = () => {
  return (
    <View style={styles.container}>
      <FlatList
        data={SETTINGS_ITEMS}
        renderItem={({ item }) => <SettingsListItem item={item} />}
        keyExtractor={(item) => item.id.toString()}
      />
    </View>
  );
};

export default Settings;
