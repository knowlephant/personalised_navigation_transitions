import EStyleSheet from 'react-native-extended-stylesheet';
import { Dimensions } from 'react-native';

const VIEW_WIDTH = Dimensions.get('window').width;

const styles = EStyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  navigationContainer: {
    flex: 1,
    alignSelf: 'center',
    flexWrap: 'wrap',
    width: VIEW_WIDTH - 100,
    marginTop: 100,
    flexDirection: 'row',
  },
});

export default styles;
