import React from 'react';
import { View } from 'react-native';

import IconLabelButton from '../UI/IconLabelButton';

import styles from './styles';

const feed = require('../../assets/icons/feed.png');
const gallery = require('../../assets/icons/gallery.png');
const profile = require('../../assets/icons/profile.png');
const settings = require('../../assets/icons/settings.png');

const NAVIGATION_ITEMS = [
  {
    title: 'Explore',
    navigateTo: 'Explore',
    icon: feed,
  },
  {
    title: 'Gallery',
    navigateTo: 'Gallery',
    icon: gallery,
  },
  {
    title: 'Profile',
    navigateTo: 'Profile',
    icon: profile,
  },
  {
    title: 'Settings',
    navigateTo: 'Settings',
    icon: settings,
  },
];

const Navigation = ({ navigation, route }) => {
  const handleSelection = (item) => {
    navigation.navigate('Main', {
      screen: item.navigateTo,
    });
  };

  return (
    <View style={styles.container}>
      <View style={styles.navigationContainer}>
        {NAVIGATION_ITEMS.map((item, index) => (
          <IconLabelButton
            key={index.toString()}
            icon={item.icon}
            label={item.title}
            onPress={() => handleSelection(item)}
            isVertical
            uppercase
            height={100}
          />
        ))}
      </View>
    </View>
  );
};

export default Navigation;
