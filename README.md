# Personalised Zoom Navigation

Personalised Zoom Navigatoin is a starter kit to help you create custom navigation transitions in your React Native applications.

For further documentation, see the following links:

[How To Personalise Your App’s Navigation Experience with React Native](https://medium.com/javascript-in-plain-english/personalised-navigation-transition-effects-with-react-native-fc1493a4b7a9)

## Installation

This resource is managed with Expo:

```bash
cd [your-project-name]

yarn install

yarn start
```

## License

No restrictions apply
